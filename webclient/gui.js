(function x(v){
  if(!$('#statusbars').attr('done')){ // initialize gui if food bar doesn't exist yet
    $('#client')[0].append($('#inputbar')[0],$('#statusbars')[0]);
    $('#controls')[0].append($('#status')[0]);
    $('#sidebar')[0].append($('#controls')[0]);
    $('#statusbars')[0].innerHTML=[      //reorder (or even delete) the following, but don't change their names (you won't see the names anywhere anyway)!
      ['health','760000','FFB5B5',null],
      ['food','9F5E00','FFD598',null],
      ['drink','949000','FFFC96',null],
      ['fatigue','065F00','AEE1AB',null],
      ['mana','243F81','B7C9F8',null],
      ['intox','243F81','B7C9F8',null],
      ['pro','3F2261','CAB3E6',null]
    ].map(function(x){
      return `<div id='${x[0]}' style='width: calc(16.8% - 15px);'><div class='progress' style='background-color: #${x[1]};'></div><div class='text' style="font-weight: bold;color: #${x[2]||'d0d0d0'};text-shadow:0px 0px #${x[3]||'000'}"></div></div>`}).join('');
    $('#inputbar')[0].style.width="calc(60% - 22px)";
    $('#statusbars')[0].style.bottom="55px";
    $('#controls')[0].style.padding=$('#inputbar')[0].style.bottom="3px";
    $('#status')[0].style.paddingRight=$('#connect')[0].style.position=$('#controls')[0].style.width=$('#controls')[0].style.right=$('#opensettings')[0].style.position='initial';$('#statusbars').attr('done','true')}
  // initialization is done at this point - now let's update the bars (except health, mana and fatigue)
  if($("#food")[0]) {
  $("#food .progress")[0].style.width=''+(" orem".indexOf(v.food[11])*25)+'%';
  $("#food .text")[0].innerText=v.food;}
  if($("#drink")[0]) {
  $("#drink .progress")[0].style.width=''+("amol ".indexOf(v.drink[12])*25)+'%';
  $("#drink .text")[0].innerText=v.drink;}
  if($("#intox")[0]) {
  $("#intox .progress")[0].style.width=''+("amol ".indexOf(v.drink[12])*25)+'%';
  $("#intox .text")[0].innerText=v.drink;}
  if($("#pro")[0]) {
  $("#pro .text")[0].innerText=v.progress;
  $("#pro .progress")[0].style.width=`${"n isatmnsilwalsmmddcncgovrmjgeetaeimtefn".indexOf(v.progress[0]+v.progress[2])*2.7}%`}})(mud.gmcp['char.vitals']);


if(!$("#lkb")[0]) $("#topsidebar .idTabs")[0].innerHTML+='<li style="padding: 5px;" id="lkb"></li>';
$("#lkb")[0].innerHTML=`LKB: ${(args[1].match(/(\d+) platinum/)||[0,0])[1]}P`;